<div class="map-container map-$TemplateID"<% if $TemplateID %> id="{$TemplateID}"<% end_if %>>
	<div class="map" data-fit-bounds="true"></div>
	<% loop $Children %>
		<% if $Lat && $Lng %>
			<div itemscope class="places" data-id="mapitem-{$TemplateID}">
				<meta itemprop="content" content="$InfoContent"/>
				<meta itemprop="latitude" content="$Lat"/>
				<meta itemprop="longitude" content="$Lng"/>
			</div>
		<% end_if %>
	<% end_loop %>
</div>
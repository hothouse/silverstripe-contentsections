<% if $DisplayTitle %>
    <h3 class="chart-title">$Title</h3>
<% end_if %>
<% if $ChartWidth || $ChartHeight %>
    <div style="<% if $ChartWidth %>width:{$ChartWidth}px; <% end_if %><% if $ChartHeight %>height:{$ChartHeight}px; <% end_if %>position: relative;">
		<canvas <% if $TemplateID %> id="{$TemplateID}_canvas"<% end_if %>></canvas>
    </div>
<% else %>
	<canvas <% if $TemplateID %> id="{$TemplateID}_canvas"<% end_if %>></canvas>
<% end_if %>

<script type="application/javascript">
	function waitForjQuery(method) {
		if (window.jQuery)
			method();
		else
			setTimeout(function() { waitForjQuery(method) }, 50);
	}

	waitForjQuery(function () {
		$(document).ready(function() {
			var displayLegend = '{$ChartType}' != 'bar';
			var pieData = {$PieNumbers};
			var colours = {$PieColours};
			var hovers = {$PieHovers};
			var pieOptions = {
				animateScale : true,
				responsive: true,
				maintainAspectRatio: false,
				legend: {
					display: displayLegend,
					position: '{$LegendPosition}'
				}
			};
			var pieLabels = {$PieLabels};

			var myCanvas = $("#{$TemplateID}_canvas");

			new Chart(myCanvas, {
				type: '{$ChartType}',
				data: {
					labels: pieLabels,
					datasets: [{
						data: pieData,
						backgroundColor: colours,
						hoverBackgroundColor: hovers
					}]
				},
				options: pieOptions
			});
		});
	});
</script>
<% loop $ContentSections %>
	<div class="content-section content-section-$ID content-section-$Type.LowerCase content-section-$Type.LowerCase-{$ContentType.CustomTemplateCSSClass}<% if $BackgroundImage %> background-image<% end_if %>"<% if $BackgroundImage || $BackgroundColour %> style="<% if $BackgroundImage %>background-image: url('$BackgroundImage.URL');<% end_if %><% if $BackgroundColour %> background-color: #{$BackgroundColour};<% end_if %>"<% end_if %>>
		<div>
			<div class="container<% if $Width %>-$Width<% end_if %>">
				<% if $DisplayHeading %>
					<h2 class="text-center">
						$Title
					</h2>
				<% end_if %>
				$Section
				<% include ContentSectionLinks %>
				$LinkModals
			</div>
		</div>
	</div>
<% end_loop %>

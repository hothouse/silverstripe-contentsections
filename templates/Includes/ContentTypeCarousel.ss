<div<% if $TemplateID %> id="{$TemplateID}"<% end_if %> class="carousel carousel-{$TemplateID(force)} slide carousel-$Transition"<% if $AutoStart %> data-ride="carousel"<% end_if %>>
	<% if $Children.Count > 1 %>
		<ol class="carousel-indicators">
			<% loop $Children %>
				<li data-target=".carousel-{$Up.TemplateID(force)}" data-slide-to="$Pos(0)"<% if $First %> class="active"<% end_if %>></li>
			<% end_loop %>
		</ol>
	<% end_if %>

	<div class="carousel-inner" role="listbox">
		<% loop $Children %>
			<% if $ClassName == 'ContentTypeImage' %>
				<% include ContentTypeCarouselItemImage %>
			<% else %>
				<div class="item<% if $First %> active<% end_if %>">
					$Element
				</div>
			<% end_if %>
		<% end_loop %>
	</div>

	<% if $Children.Count > 1 %>
		<a class="left carousel-control" href="#" data-target=".carousel-{$TemplateID(force)}" role="button" data-slide="prev">
			<i class="fa fa-angle-left glyphicon-chevron-left" aria-hidden="true"></i>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#" data-target=".carousel-{$TemplateID(force)}" role="button" data-slide="next">
			<i class="fa fa-angle-right glyphicon-chevron-right" aria-hidden="true"></i>
			<span class="sr-only">Next</span>
		</a>
	<% end_if %>
</div>

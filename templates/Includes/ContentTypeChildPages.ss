<div class="row"<% if $TemplateID %> id="{$TemplateID}"<% end_if %>>
	<% loop $ChildPages %>
		<div class="col-sm-6<% if $TotalItems > 1 %> col-md-$Top.ColWidth<% end_if %>">
			<div class="text-center">
				<% if $Image %>
					<a href="$Link">
						<img class="img-responsive" alt="$Title.XML" src="$FeaturedImage.Fill(400, 210).URL"/>
					</a>
				<% end_if %>
				<h3>
					$Title
				</h3>
				<% if $Summary %>
					<p>
						$Summary.LimitCharacters(50)
					</p>
				<% end_if %>
				<a class="btn btn-default" href="$Link">
					$MenuTitle
				</a>
			</div>
		</div>
	<% end_loop %>
</div>

var
	loadFlickrGallery = function () {
		var
			$flickrgallery = $(this),
			flickerAPI = "https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?",
			limit = $flickrgallery.data('limit'),
			initMe = function() {
				$flickrgallery.justifiedGallery({
					rowHeight: 170
				});
				var
					tags = $flickrgallery.data('tags'),
					tags = tags ? '&tags=' + tags.replace(", ", "%20") : '',
					id = $flickrgallery.data('flickrid'),
					id = id ? '&user_id=' + id : '';

				$('a[data-rel^=lightcase]').lightcase({
					transition: 'scrollHorizontal',
					swipe: true,
					caption: '<i class="fa fa-flickr"></i> <a href="https://www.flickr.com/search/?' + id + tags + '&sort=date-taken-desc' + '&view_all=1&tag_mode=any" class="flickr-link">See more on Flickr</a>'
				});
			};
		$.getJSON(flickerAPI, {
			tags: $flickrgallery.data('tags'),
			tagmode: "any",
			format: "json",
			id: $flickrgallery.data('flickrid')
		}).done(function (data) {
			$.each(data.items, function (i, item) {
				if (i === limit) {
					return false;
				}
				title = item.title;
				imgURL = item.media.m;
				imgThumb = imgURL.replace('_m', '_n')
				imgLarge = imgURL.replace('_m', '_b');
				$flickrgallery.append('<a href="' + imgLarge + '" data-rel="lightcase:flickr" title="' + title + '"><img src="' + imgThumb + '" /></a>');
			});
			initMe();
		});

		// init
		$('a[data-toggle="tab"]').on('shown.bs.tab', initMe);
	};

$(document).ready(function () {
	setTimeout(function () {
		$('.flickrgallery').each(loadFlickrGallery);
	}, 200);
});

var
	loadImageGallery = function () {
		var
			$imagegallery = $(this).find('.image-gallery'),
			$thumbnails = $(this).find('.image-gallery-thumbnails'),
			initMe = function() {
				// swipe
				$imagegallery.swiperight(function () {
					$(this).carousel('prev');
				});
				$imagegallery.swipeleft(function () {
					$(this).carousel('next');
				});

				// thumbs
				$imagegallery.on('slid.bs.carousel', function () {
					var index = $(this).find('.item.active').index();
					$('[id^=carousel-selector-]')
						.removeClass('selected').addClass('not-selected')
						.eq(index)
						.addClass('selected').removeClass('not-selected')
				});
				$('[id^=carousel-selector-]').click( function(){
					$(this).addClass('selected').removeClass('not-selected').siblings().not(this).removeClass('selected').addClass('not-selected');
					var id_selector = $(this).attr("id").split('-');
					var id = id_selector[id_selector.length - 1];
					$imagegallery.carousel(parseInt(id) - 1);
				});
				$thumbnails.find('img').on({
					mouseenter: function () {
						$(this).addClass('active').parent().siblings().find('img').not(this).addClass('inactive');
					},
					mouseleave: function () {
						$(this).removeClass('active').parent().siblings().find('img').not(this).removeClass('inactive');
					}
				});

				// justify
				$thumbnails.justifiedGallery();
			};
		initMe();

		// init
		$('a[data-toggle="tab"]').on('shown.bs.tab', initMe);
	};

$(document).ready(function () {
	setTimeout(function () {
		$('.content-section-imagegallery, .content-type-imagegallery').each(loadImageGallery);
	}, 200);
});

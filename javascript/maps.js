// maps
var
	useInfoBubble = typeof InfoBubble === 'function',
	infoBubble = {},
	useOMS = typeof OverlappingMarkerSpiderfier === 'function',
	// map bounds to whole NZ
	autoBoundsLimit = autoBoundsLimit || new google.maps.LatLngBounds(
		new google.maps.LatLng(
			-45, 160
		),
		new google.maps.LatLng(
			-33, 180
		)
	),
	mapOptions = mapOptions || {},
	infoBubbleStyle = infoBubbleStyle || {},
	markerClustererIconWidth = 95,
	markerClustererIconHeight = 95,
	markerClustererOptions = {
		gridSize: 25,
		maxZoom: 14,
		//minimumClusterSize: 10,
		clusterClass: 'map-marker-cluster',
		styles: [{
			url: 'silverstripe-contentsections/images/empty.png',
			width: Math.floor(markerClustererIconWidth / 3),
			height: Math.floor(markerClustererIconWidth / 3),
			textColor: '#ffffff',
			textSize: 14
		}, {
			url: 'silverstripe-contentsections/images/empty.png',
			width: Math.floor(markerClustererIconWidth / 2.5),
			height: Math.floor(markerClustererIconHeight / 2.5),
			textColor: '#ffffff',
			textSize: 14
		}]
	},
// map markers
	createMapMarker = function (item, numMarkersToLoad, map, markerClusterer, oms, mapBounds, mapOptions) {
		item.Icon = item.Icon || 'fa-map-marker';
		if (item.IconClass) {
			item.Icon += ' ' + item.IconClass;
		}
		labelContent = '<i class="fa ' + item.Icon + '"></i>';

		var marker = new MarkerWithLabel({
			position: new google.maps.LatLng(parseFloat(item.latitude), parseFloat(item.longitude)),
			icon: ' ',
			bounds: true,
			map: map,
			content: item.content || '',
			labelContent: labelContent,
			labelAnchor: new google.maps.Point(16, 52),
			labelClass: 'map-marker-label'
		});

		// add to map
		if (useOMS) {
			oms.addMarker(marker);
		} else {
			(function (marker) {
				google.maps.event.addListener(marker, 'click', function () {
					if (useInfoBubble && marker.content) {
						infoBubble.open(map, marker);
						infoBubble.setContent(marker.content)
					}
				});
			})(marker);
		}

		// add to clusterer
		markerClusterer.addMarker(marker);

		// add to bounds & center map
		if (mapBounds !== null) {
			if (item.latitude && item.longitude && autoBoundsLimit.contains(marker.position)) {
				mapBounds.extend(marker.position);
			}
			if (numMarkersToLoad == 0) {
				// Don't zoom in too far on only one marker
				if (mapBounds.getNorthEast().equals(mapBounds.getSouthWest())) {
					var extendPoint1 = new google.maps.LatLng(mapBounds.getNorthEast().lat() + 0.1, mapBounds.getNorthEast().lng() + 0.1);
					var extendPoint2 = new google.maps.LatLng(mapBounds.getNorthEast().lat() - 0.1, mapBounds.getNorthEast().lng() - 0.1);
					mapBounds.extend(extendPoint1);
					mapBounds.extend(extendPoint2);
				}
				map.fitBounds(mapBounds);

				// open marker if we have only one
				allMarkers = markerClusterer.getMarkers();
				if(allMarkers.length == 1 && !mapOptions.hasOwnProperty('noAutoOpen')) {
					if (useOMS) {
						oms.trigger('click', marker);
					} else {
						new google.maps.event.trigger(marker, 'click');
					}
				}
			}
		}
	},
// maps loading
	loadMap = function () {
		var
			$MapCanvas = $(this),
			oms,
			mapZoomLevel,
			mapBounds,
			markerObjects = [],
			markerClusterer;

		mapOptions = $.extend({
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}, mapOptions);
		infoBubbleStyle = $.extend({
			ShadowStyle: 1,
			padding: 0,
			borderRadius: 0,
			borderWidth: 0,
			minWidth: 280,
			maxWidth: 280,
			minHeight: 300,
			maxHeight: 300,
			arrowSize: 5,
			backgroundClassName: 'map-info-bubble'/*,
			hideCloseButton: true*/
		}, infoBubbleStyle);

		// determine initial map center
		if ($MapCanvas.attr('data-latitude') && $MapCanvas.attr('data-longitude')) {
			var mapCenter = new google.maps.LatLng($MapCanvas.attr('data-latitude'), $MapCanvas.attr('data-longitude'));
		} else {
			var mapCenter = new google.maps.LatLng(-41.4, 172.8);
		}
		$.extend(mapOptions, {
			center: mapCenter
		});

		// determine initial map zoom
		if ($MapCanvas.attr('data-zoom')) {
			var dataZoom = parseInt($MapCanvas.attr('data-zoom'));
			if (dataZoom) {
				$.extend(mapOptions, {
					zoom: dataZoom
				});
			}
		}

		// map
		var map = new google.maps.Map(
			$MapCanvas[0], mapOptions
		);

		// bubble
		if (useInfoBubble) {
			infoBubbleStyle = $.extend({
				map: map
			}, infoBubbleStyle);
			infoBubble = new InfoBubble(infoBubbleStyle);
		}

		// add listener to close bubble on click
		google.maps.event.addListener(map, 'click', function () {
			if (useInfoBubble) {
				infoBubble.close();
			}
		});
		google.maps.event.addListener(map, 'zoom_changed', function () {
			newMapZoomLevel = map.getZoom();
			if (useInfoBubble && newMapZoomLevel < mapZoomLevel) {
				infoBubble.close();
			}
			mapZoomLevel = newMapZoomLevel;
		});
		google.maps.event.addDomListener(window, 'resize', function () {
			var center = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(center);
		});

		// make sure we init on tab show
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			google.maps.event.trigger(map, 'resize');
			if (mapBounds !== null) {
				map.fitBounds(mapBounds);
			} else {
				var center = new google.maps.LatLng($MapCanvas.attr('data-latitude'), $MapCanvas.attr('data-longitude'));
				map.setCenter(center);
			}
		});

		// spider
		if (useOMS) {
			oms = new OverlappingMarkerSpiderfier(map, {
				markersWontMove: true,
				markersWontHide: true,
				keepSpiderfied: true
			});
			oms.addListener('click', function (marker) {
				if (useInfoBubble && marker.content) {
					infoBubble.open(map, marker);
					infoBubble.setContent(marker.content);
				}
			});
		}

		// cluster the markers
		markerClusterer = new MarkerClusterer(map, [], markerClustererOptions);

		// get data & prepare
		$MapCanvas.parent().find('.places[itemscope]').each(function (index, item) {
			var markerObject = {};
			$(this).children().each(function (index, item) {
				var
					$this = $(this),
					content = $this.attr('content'),
					itemProp = $this.attr('itemprop');

				if (itemProp) {
					markerObject[itemProp] = content;
				}
			});

			markerObjects.push(markerObject);
		});

		// fit bounds when finished
		if ($MapCanvas.attr('data-fit-bounds') == 'true') {
			mapBounds = new google.maps.LatLngBounds();
		} else {
			mapBounds = null;
		}

		// load markers
		numMarkersToLoad = markerObjects.length;
		$(markerObjects).each(function (index, item) {
			numMarkersToLoad--;
			createMapMarker(item, numMarkersToLoad, map, markerClusterer, oms, mapBounds, mapOptions);
		});

		$(window).trigger('CsGoogleMapInitialized', map);
	};

$(document).ready(function () {
	setTimeout(function () {
		$('.map').each(loadMap);
	}, 200);
});
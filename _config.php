<?php

define('MODULE_CONTENTSECTIONS_PATH', basename(dirname(__FILE__)));

ShortcodeParser::get('default')->register('toc', array('ContentSection', 'ToC'));
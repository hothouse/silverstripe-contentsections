<?php

class ContentTypeGridFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest {

	public function doSave($data, $form) {
		// call parent
		$Response = parent::doSave($data, $form);

		// we have to tackle this here because the parent function calls $list->add AFTER write
		// so that the parent id always gets reset to the original value
		if(
			$this->record->ID != 0
			&& is_a($this->record, 'ContentType')
			&& $this->record->canEdit()
		) {

			$redirect = false;
			if(isset($data['ParentID']) && $data['ParentID'] != $this->record->ParentID) {
				$this->record->ParentID = $data['ParentID'];
				$this->record->write();
				$redirect = true;
			}

			// reset content section if content type has been moved
			if($this->record->ParentID != 0 && $ContentSection = ContentSection::get()->filter('ContentTypeID', $this->record->ID)->first()) {
				$ContentSection->ContentTypeID = 0;
				$ContentSection->write();
				$redirect = true;
			}

			// redirect
			if($redirect) {
				$controller = Controller::curr();
				$controller->getRequest()->addHeader('X-Pjax', 'Content');
				$item = $this->record->getTopLevelItem();
				return $controller->redirect($item->CMSEditLink(), 302);
			}
		}

		return $Response;
	}

}
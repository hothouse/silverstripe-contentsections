<?php

class ContentTypeCarousel extends ContentTypeContainer {

	private static $allowed_content_types = array(
		'ContentTypeGrid',
		'ContentTypeMap',
		'ContentTypeImage',
		'ContentTypeText',
		'ContentTypeVideo'
	);

	private static $db = array(
		'AutoStart' => 'Boolean',
		'Transition' => "Enum('slide, fade', 'slide')"
	);

	private static $defaults = array(
		'AutoStart' => true
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		if(!$this->ParentID) {
			$fields->removeByName('BackgroundImage');
		}
		return $fields;
	}

	public function forTemplate() {
		return parent::forTemplate();
	}

	public function implementFrontend() {
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/jquery.mobile.custom.min.js');
	}
}
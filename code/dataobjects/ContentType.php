<?php

class ContentType extends DataObject {

	private static $db = array(
		'Title' => 'Varchar(255)',
		'GridColWidth' => 'Int',
		'SortID' => 'Int',
		'CustomTemplate' => 'Varchar',
		'CustomTemplateClass' => 'Varchar'
	);

	private static $summary_fields = array(
		'Type' => 'Type',
		'Title' => 'Title'
	);

	private static $default_sort = 'SortID';

	public function populateDefaults() {
		$this->GridColWidth = Config::inst()->get('ContentSections', 'default_grid_col_width');
		parent::populateDefaults();
	}

	// tree like list of ContentType for the dependent drop down
	// lists all elements that can be linked to per page
	public static function get_content_elements_for_page($PageID) {
		// prepare return list
		// provide empty value to enable saving of the form
		$Return = array('');

		if ($Page = Page::get()->byID($PageID)) {

			// run through all sections
			foreach ($Page->ContentSections()->exclude('ContentTypeID', 0)->sort('SortID') as $ContentSection) {
				$Return[$ContentSection->ContentTypeID] = $ContentSection->Title.' (Section)';
				if (is_a($ContentSection->ContentType(), 'ContentTypeContainer')) {
					foreach ($ContentSection->ContentType()->Children() as $ContentType) {
						$Return[$ContentType->ID] = $ContentType->getBreadcrumbs();
					}
				}
			}
		}

		return $Return;
	}

	// tree like list of ContentTypeContainer records for the dependent drop down
	// candidate container records for another record to be the parent
	public static function get_content_containers_for_section($ContentSectionID) {
		$Request = Controller::curr()->getRequest();
		if (is_numeric($ContentSectionID) && $ContentSection = ContentSection::get()->byID($ContentSectionID)) {
			// current content type
			$ContentType = $ContentSection->ContentType();

			// if it exists, check further
			if ($ContentType->exists()) {

				// prepare return list
				$Return = array();

				// if we list the currently edited record, make sure we can leave it on top level
				if ($Request->param('ID') == $ContentType->ID) {
					$Return[0] = 'top level';
				}

				// get all children container if we have a container as content type
				if (is_a($ContentType, 'ContentTypeContainer')) {
					$List = array();

					// if we don't list the currently edited record, list it as well
					if ($Request->param('ID') != $ContentType->ID) {
						$Return[$ContentType->ID] = $ContentType->getBreadcrumbs();
					}

					// add children
					$ContentType->ChildrenContainer($List);
					$Return += $List;
				}
				return $Return;
			} else {
				// only if it does not exists, allow setting on top level
				return array(0 => 'top level');
			}
		}
	}

	public function creatableContentTypes($CheckSectionPermission = false) {
		if ($fields = ClassInfo::subclassesFor('ContentType')) {
			array_shift($fields); // get rid of subclass 0
			arsort($fields); // sort

			// white list
			$WhiteList = $this->stat('allowed_content_types');

			// return
			$return = array();
			foreach ($fields as $field) {
				// never create a 'container' as this is only a parent element
				if ($field == 'ContentTypeContainer') {
					continue;
				}

				// check if class allows this type
				if (is_array($WhiteList) && count($WhiteList) && !in_array($field, $WhiteList)) {
					continue;
				}

				// check if white listed by admin
				if ($CheckSectionPermission) {
					if (singleton($field)->canCreateSection() !== true) {
						continue;
					}
				} else {
					if (singleton($field)->canCreate() !== true) {
						continue;
					}
				}

				// add to list
				$return[$field] = str_replace('ContentType', '', $field);
			}

			return $return;
		}

		return false;
	}

	public function creatableContentSectionTypes() {
		return $this->creatableContentTypes(true);
	}


	public function creatableContainerTypes() {
		if ($fields = ClassInfo::subclassesFor('ContentTypeContainer')) {
			array_shift($fields); // get rid of subclass 0
			arsort($fields); // sort

			// return
			$return = array();
			foreach ($fields as $field) {
				// check if white listed by admin
				if (singleton($field)->canCreate() !== true) {
					continue;
				}

				// add to list
				$return[$field] = str_replace('ContentType', '', $field);
			}

			return $return;
		}

		return false;
	}

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('SortID');
		$fields->removeByName('ParentID');
		if (
			!$this->ParentID
			|| ($this->Parent()->ClassName != 'ContentTypeGrid' && $this->Parent()->ClassName != 'ContentTypeSnippetGrid')
			|| !$this->Parent()->ManualColWidth
		) {
			$fields->removeByName('GridColWidth');
		} else {
			$array = array();
			for ($i = 1; $i < 13; $i++) {
				$array[$i] = $i;
			}
			$fields->addFieldToTab(
				'Root.Main',
				DropdownField::create(
					'GridColWidth',
					'Manual column width in grid',
					$array
				)
			);
		}

		// add template/css class name chooser only if the parent element uses the default template
		if ($this->Parent()->exists() && $this->Parent()->HasCustomTemplate()) {
			$fields->removeByName('CustomTemplate');
			$fields->removeByName('CustomTemplateClass');
		} else {
			// add template chooser
			$theme = Config::inst()->get('SSViewer', 'theme');
			$manifest = SS_TemplateLoader::instance()->getManifest();
			$templates = $manifest->getTemplates();
			$ClassName = strtolower($this->ClassName);
			$templatesList = array();
			foreach ((array)$templates as $key => $template) {
				$parts = explode('_', $key);
				if ($key == $ClassName || (count($parts) == 2 && $ClassName == $parts[0])) {
					$candidates = $manifest->getCandidateTemplate($key, $theme);
					if (count($candidates) && isset($candidates['Includes'])) {
						$templateName = str_replace('.ss', '', basename($candidates['Includes']));
						$templatesList[$templateName] = $templateName;
					}
				}
			}
			if (count($templatesList) > 1) {
				$fields->addFieldToTab(
					'Root.Settings',
					DropdownField::create('CustomTemplate', 'Custom Template', $templatesList)
				);
			} else {
				$fields->removeByName('CustomTemplate');
			}

			// add custom css class chooser if a default template is selected
			// removes this option if a custom template is selected
			// custom css classes can be set in the config.yml
			//
			//   ContentTypeGrid:
			//     default_template_css_classes:
			//       - class-one
			//       - class-two
			//
			$use_multiple_custom_classes = Config::inst()->get($this->ClassName, 'use_multiple_custom_classes');

			$customClassesList = array();
			if (!$use_multiple_custom_classes) {
				$customClassesList[] = 'default';
			}

			$customConfigClasses = (array)Config::inst()->get($this->ClassName, 'default_template_css_classes');
			if (count($customConfigClasses) > 0) {
				foreach ($customConfigClasses as $customConfigClass) {
					$customClassesList[$customConfigClass] = $customConfigClass;
				}
			}
			if (count($customClassesList) > 1) {
				if ($use_multiple_custom_classes) {
					$customClassesField = ListboxField::create(
						'CustomTemplateClass',
						'Custom Template Class',
						$customClassesList
					)
						->setMultiple(true);
				} else {
					$customClassesField = DropdownField::create(
						'CustomTemplateClass',
						'Custom Template Class',
						$customClassesList
					);
				}

				$fields->addFieldToTab(
					'Root.Settings',
					$customClassesField
						->displayUnless('CustomTemplate')->isNotEqualTo($this->ClassName)->end()
				);
			} else {
				$fields->removeByName('CustomTemplateClass');
			}
		}

		return $fields;
	}

	public function getTopLevelID() {
		return $this->getTopLevelItem()->ID;
	}

	public function getTopLevelItem() {
		$type = $this;
		while ($type->ParentID && ContentSection::get()->filter('ContentTypeID', $type->ID)->Count() == 0) {
			$type = $type->Parent();
		}
		return $type;
	}

	public function getBreadcrumbs($separator = ' -> ') {
		$crumbs = array();
		$object = $this;
		$crumbs[] = $object->getTreeTitle();
		while ($object->ParentID) {

			$object = $object->Parent();
			$crumbs[] = $object->getTreeTitle();
		}
		return implode($separator, array_reverse($crumbs));
	}

	public function getType() {
		return str_replace('ContentType', '', $this->ClassName);
	}

	public function forTemplate() {
		return $this->RenderWith($this->TemplateName());
	}

	public function Element($Template = 'ContentType') {
		return $this->renderWith($Template);
	}

	public function TemplateName() {
		$Template = $this->CustomTemplate ? $this->CustomTemplate : $this->ClassName;
		return $Template;
	}

	public function getTitle() {
		return $this->ParentID || !is_a($this, 'ContentTypeContainer') ? $this->getField('Title') : $this->getType();
	}

	public function getTreeTitle() {
		return $this->ParentID ? $this->getField('Title').' ('.$this->getType().')' : $this->getType();
	}

	function canCreate($member = null) {
		if (Permission::check('ADMIN')) {
			return true;
		}

		$SiteConfig = SiteConfig::current_site_config();
		if ($SiteConfig && $SiteConfig->exists()) {
			$whitelisted = explode(',', $SiteConfig->ContentTypeWhiteList);
			if (!in_array($this->class, $whitelisted)) {
				return false;
			}
		}

		return parent::canCreate($member);
	}


	function canCreateSection($member = null) {
		if (Permission::check('ADMIN')) {
			return true;
		}

		$SiteConfig = SiteConfig::current_site_config();
		if ($SiteConfig && $SiteConfig->exists()) {
			$whitelisted = explode(',', $SiteConfig->ContentSectionWhiteList);
			if (!in_array($this->class, $whitelisted)) {
				return false;
			}
		}

		return parent::canCreate($member);
	}

	public function Section() {
		return ContentSection::get()->filter('ContentTypeID', $this->getTopLevelID())->first();
	}

	// todo!
	// currently only gets the page link, not the deeper item edit link
	public function CMSEditLink() {
		if ($ContentSection = ContentSection::get()->filter('ContentTypeID', $this->getTopLevelID())->first()) {
			return $ContentSection->Pages()->first()->CMSEditLink();
		}
	}

	// create list of other possible uses of ids
	// and store it for later access
	private static $site_wide_id_list = array();
	public static function get_site_wide_id_list() {
		// get from cache
		// on dev sites we are caching this per per page call
		// on live sites we use the in built cache factory
		// todo: this should check for the actual cache lifetime not depending on the environment
		if (Director::isDev()) {
			$site_wide_id_list = self::$site_wide_id_list;
		} else {
			$cachename = $cachekey = 'site_wide_id_list';
			$cache = SS_Cache::factory($cachename, 'Output', array('automatic_serialization' => true));
			$site_wide_id_list = $cache->load($cachekey);
		}

		// check if existent
		if (!$site_wide_id_list || !count($site_wide_id_list)) {

			// filter instance
			$f = new URLSegmentFilter();

			// add sections that are attached to a page
			$list = SQLSelect::create(
				'Title, ContentTypeID',
				$from = array('ContentSection'),
				'Title != \'\''
			)
				->setDistinct(false)
				->addInnerJoin(
					'Page_ContentSections',
					'Page_ContentSections.ContentSectionID = ContentSection.ID'
				);

			$SectionContentTypeIDs = array();
			foreach ($list->execute() as $item) {
				$site_wide_id_list[] = $f->filter($item['Title']);
				$SectionContentTypeIDs[] = $item['ContentTypeID'];
			}

			// add content elements that are on sections that are attached to a page
			// using self left join query for performance reasons
			$Table = <<<SQL
				(
					SELECT
						C1.ID as ID,
						C1.Title as Title,
						COALESCE(
							NULLIF(C5.ParentID, 0),
							NULLIF(C4.ParentID, 0),
							NULLIF(C3.ParentID, 0),
							NULLIF(C2.ParentID, 0),
							NULLIF(C1.ParentID, 0)
						) AS TopMostContentTypeID
					FROM
						ContentType AS C1
						LEFT JOIN ContentType AS C2 ON (C1.ParentID = C2.ID)
						LEFT JOIN ContentType AS C3 ON (C2.ParentID = C3.ID)
						LEFT JOIN ContentType AS C4 ON (C3.ParentID = C4.ID)
						LEFT JOIN ContentType AS C5 ON (C4.ParentID = C5.ID)
					WHERE 
						C1.Title != ''
				) List
SQL;

			$list = SQLSelect::create(
				'List.ID, List.Title',
				$Table,
				sprintf('List.TopMostContentTypeID IN (%1$s)', implode(',', $SectionContentTypeIDs))
			);
			foreach ($list->execute() as $item) {
				$site_wide_id_list[] = $f->filter($item['Title']);
			}

			// add site tree url segments
			$list = SiteTree::get()->column('URLSegment');
			foreach ($list as $item) {
				$site_wide_id_list[] = $item;
			}

			// cache it
			if (Director::isDev()) {
				self::$site_wide_id_list = $site_wide_id_list;
			} else {
				$cache->save($site_wide_id_list, $cachekey);
			}
		}
		return $site_wide_id_list;
	}

	// unique id for template use, includes ID and current page id
	public function TemplateID($forceID = false) {

		// init empty id
		$TemplateID = '';

		// don't print an id if our parent is a tab container
		// unless we are displaying the tab nav
		if (!$forceID && (is_a($this->Parent(), 'ContentTypeTabs') || is_a($this->Parent(), 'ContentTypeAccordion'))) {
			return '';
		}

		// get section
		$Section = $this->Section();

		// determine the title of the current element
		if ($this->ParentID) {
			$Title = $this->getField('Title');
		} else {
			$Title = $Section->getField('Title');
		}

		// create the id out of title
		$AddHash = false;
		if ($Title) {
			$TemplateID = Convert::raw2url($Title);

			// find out if this id could be used somewhere else
			$uses = array_keys(self::get_site_wide_id_list(), $TemplateID);

			// if used somewhere else, add hash
			if (count($uses) > 1) {
				$TemplateID .= '-';
				$AddHash = true;
			}
		} else {
			$AddHash = true;
		}

		// add hash if not unique
		if ($AddHash) {
			$TemplateID .= substr(md5($Section->ID.'-'.$this->ID), 0, 5);
		}

		return $TemplateID;
	}

	public function onAfterWrite() {
		parent::onAfterWrite();
		// reset content section if content type has been moved
		if (
			$this->ParentID != 0
			&& $ContentSection = ContentSection::get()->filter('ContentTypeID', $this->ID)->first()
		) {
			// this gets overridden by the HasOneButtonRelationList add function
			// see ContentTypeGridFieldDetailForm_ItemRequest where this is fixed
			$ContentSection->ContentTypeID = 0;
			$ContentSection->write();
		} elseif (
			$this->ContentSectionID
			&& $this->ParentID == 0
			&& $ContentSection = ContentSection::get()->byID($this->ContentSectionID)
		) {
			$ContentSection->ContentTypeID = $this->ID;
			$ContentSection->write();
		}

		/**
		 * if any frontend js/css needed, save for later use
		 */
		$siteConfig = SiteConfig::current_site_config();
		$uses = $siteConfig->ContentTypeFrontendUses;
		if (strlen($uses)) {
			$uses = unserialize($uses);
		} else {
			$uses = array();
		}
		if ($this->Section()) {
			if (method_exists($this, 'implementFrontend')) {
				if (!isset($uses[$this->Section()->ID])) {
					$uses[$this->Section()->ID] = array();
				}
				$uses[$this->Section()->ID][] = $this->ID;

				$siteConfig->ContentTypeFrontendUses = serialize($uses);
				$siteConfig->write();
			} else {
				if (isset($uses[$this->Section()->ID])) {
					foreach ($uses[$this->Section()->ID] as $key => $id) {
						if ($id == $this->ID) {
							unset($uses[$this->Section()->ID][$key]);
						}
					}
				}
			}
		}

		$this->reSavePage();
	}

	public function ColWidth() {
		if ($Parent = $this->Parent()) {
			if ($Parent->ManualColWidth) {
				return $this->GridColWidth;
			} else {
				return $Parent->ColWidth();
			}
		}
	}

	public function HasCustomTemplate() {
		return $this->CustomTemplate && $this->CustomTemplate != $this->ClassName;
	}

	public function CustomTemplateCSSClass() {
		$CSSClass = 'default';

		// return default if parent has custom tpl
		if ($this->Parent()->exists() && $this->Parent()->HasCustomTemplate()) {
			return $CSSClass;
		}

		if ($this->HasCustomTemplate()) {
			$CSSClass = str_replace($this->ClassName, '', $this->CustomTemplate);
		} elseif ($this->CustomTemplateClass) {
			$CSSClass = $this->CustomTemplateClass;
		}
		// if multiple classes are used, separate by white space
		$use_multiple_custom_classes = Config::inst()->get($this->ClassName, 'use_multiple_custom_classes');
		if ($use_multiple_custom_classes) {
			$CSSClass = ' '.implode(' ', explode(',', $CSSClass));
			return $CSSClass;
		} else {
			return Convert::raw2url($CSSClass);
		}
	}

	public function Controller() {
		return Controller::curr();
	}

	protected function reSavePage() {
		if (Config::inst()->get('ContentType', 'resave_page_on_save')) {
			$parents = $this->parentStack();
			$topAncestor = end($parents);
			foreach (ContentSection::get()->filter('ContentTypeID', $topAncestor->ID) as $section) {
				foreach ($section->Pages() as $page) {
					$page->write();

					if (Config::inst()->get('ContentType', 'publish_page_on_save')) {
						$page->doPublish();
					}
				}
			}
		}
	}
}
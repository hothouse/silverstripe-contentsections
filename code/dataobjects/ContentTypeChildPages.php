<?php

class ContentTypeChildPages extends ContentTypeGrid {

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('ManualColWidth');
		return $fields;
	}

	public function ChildPages() {
		return Controller::curr()->Children();
	}

	public function getGridField() {
		return null;
	}

	public function ColWidth() {
		$ColWidth = ceil(12 / $this->ChildPages()->Count());
		if (!$this->MaxItemsPerRow && $defaults = $this->config()->defaults) {
			$this->MaxItemsPerRow = $defaults['MaxItemsPerRow'];
		}
		$ColWidth = max($ColWidth, 12 / $this->MaxItemsPerRow);
		return $ColWidth;
	}

}
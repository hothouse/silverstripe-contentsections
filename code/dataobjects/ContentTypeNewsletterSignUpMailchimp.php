<?php

/**
 * Created by PhpStorm.
 * User: johannesrichter
 * Date: 28/09/15
 * Time: 13:52
 */
class ContentTypeNewsletterSignUpMailchimp extends ContentType{
	private static $db = array(
		'FormAction' => 'Varchar(255)',
		'Label' => 'Varchar(100)',
		'PlaceHolder' => 'Varchar(100)'
	);

	private static $defaults = array(
		'Label' => 'Email',
		'PlaceHolder' => 'Email'
	);
}

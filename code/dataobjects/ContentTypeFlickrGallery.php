<?php

class ContentTypeFlickrGallery extends ContentType {

	private static $db = array(
		'FlickrUserID' => 'Varchar',
		'FlickrTags' => 'Varchar(255)'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		if(!$this->ParentID) {
			$fields->removeByName('Title');
		}
		$fields->addFieldToTab(
			'Root.Main',
			LiteralField::create('FlickrUserIDHint', 'Please go here to get your Flickr ID: <a href="http://idgettr.com" target="_blank">http://idgettr.com</a>'),
			'FlickrUserID'
		);
		$fields->addFieldToTab(
			'Root.Main',
			LiteralField::create('FlickrTagsHint', 'Please add tags that you have used to categorise your photos on Flickr (comma separated)'),
			'FlickrTags'
		);
		return $fields;
	}

	public function implementFrontend() {
		Requirements::css(MODULE_CONTENTSECTIONS_PATH.'/css/justifiedGallery.min.css');
		Requirements::css(MODULE_CONTENTSECTIONS_PATH.'/css/lightcase/lightcase.css');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/jquery.mobile.custom.min.js');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/justifiedgallery.js');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/lightcase.js');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/flickrgallery.js');
	}
}
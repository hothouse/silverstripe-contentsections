<?php

class ContentTypeImage extends ContentType {

	private static $db = array(
		'SubTitle' => 'Varchar(255)',
		'AltTitle' => 'Varchar(255)'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	private static $summary_fields = array(
		'Image.CMSThumbnail' => 'Image',
		'Title' => 'Title'
	);

	public function getAltTitle() {
		$return = $this->getField('AltTitle') ? $this->getField('AltTitle') : $this->Title;
		return $return;
	}
}
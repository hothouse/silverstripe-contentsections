<?php

class ContentTypeNewsletterSignUpActiveCampaign extends ContentType {

	private static $db = array(
		'Label' => 'Varchar(100)',
		'PlaceHolder' => 'Varchar(100)',
		'FormID' => 'Int',
		'ListID' => 'Int',
		'Campaign' => 'Varchar'
	);

	private static $defaults = array(
		'Label' => 'Email',
		'PlaceHolder' => 'Email'
	);

}
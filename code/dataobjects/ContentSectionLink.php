<?php

class ContentSectionLink extends DataObject {

	private static $db = array(
		'SortID' => 'Int'
	);

	private static $has_one = array(
		'ContentSection' => 'ContentSection'
	);

	private static $summary_fields = array(
		'LinkTitle' => 'LinkTitle',
		'LinkType' => 'LinkType'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('SortID');
		$fields->removeByName('ContentSectionID');
		return $fields;
	}

	public function getTitle() {
		return $this->LinkTitle;
	}

}
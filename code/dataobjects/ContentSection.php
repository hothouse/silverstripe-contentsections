<?php

class ContentSection extends DataObject {

	private static $db = array(
		'Title' => 'Varchar(255)',
		'DisplayHeading' => 'Boolean',
		'Width' => "Enum(array('small', 'medium', 'large', 'fluid'), 'large')",
		'IsActive' => 'Boolean',
		'ShowInToC' => 'Boolean',
		'BackgroundColour' => 'Varchar(6)'
	);

	private static $has_one = array(
		'ContentType' => 'ContentType',
		'BackgroundImage' => 'Image',
		'BackgroundParallax1' => 'Image',
		'BackgroundParallax2' => 'Image',
	);

	private static $has_many = array(
		'Links' => 'ContentSectionLink'
	);

	private static $belongs_many_many = array(
		'Pages' => 'Page'
	);

	private static $summary_fields = array(
		'Title' => 'Title',
		'Type' => 'Type',
		'IsActive' => 'IsActive',
		'PageTitles' => 'Pages'
	);

	private static $defaults = array(
		'IsActive' => true
	);

	private static $casting = array(
		'ToC' => 'HTMLText'
	);

	public static function ToC($arguments, $content = null, $parser = null, $tagName) {
		if (!method_exists(Controller::curr(), 'data') || !Controller::curr()->data() instanceof Page) {
			return  '';
		}
		$page = Controller::curr()->data();
		$sections = $page->ContentSectionList();

		$linkToTypes = array();

		foreach ($sections as $section) {
			if ($section->ShowInToC) {
				$linkToTypes[] = $section;
			}
		}

		$renderObject = ViewableData::create();
		$renderObject->customise(array(
				'Sections' => ArrayList::create($linkToTypes)
			)
		);

		return $renderObject->renderWith('ToC');
	}

	public function PageTitles() {
		return implode(', ', $this->Pages()->column('Title'));
	}

	public function Type() {
		return $this->ContentType()->getType();
	}

	public function getCMSFields() {
		if (!$this->ID) {
			$CreatableContentSectionTypes = singleton('ContentType')->creatableContentSectionTypes();
			if (count($CreatableContentSectionTypes)) {
				$fields = FieldList::create(
					Textfield::create('Title'),
					DropdownField::create(
						'ContentTypeClassName',
						'Content Type',
						singleton('ContentType')->creatableContentSectionTypes()
					),
					CheckboxField::create('IsActive'),
					CheckboxField::create('ShowInToC', 'Show in Table of Content')
				);
			} else {

				$fields = FieldList::create(
					LiteralField::create('NoSectionsAllowed', 'No new sections can be created.')
				);
			}
		} else {
			$fields = parent::getCMSFields();

			// links grid
			$config = $fields->dataFieldByName('Links')->getConfig();
			$config->addComponent(new GridFieldOrderableRows('SortID'));
			$config->removeComponent($config->getComponentByType('GridFieldDeleteAction'));
			$config->addComponent(new GridFieldDeleteAction(false));
			$config->removeComponent($config->getComponentByType('GridFieldAddExistingAutocompleter'));

			// remove content type
			$fields->removeByName('ContentTypeID');

			// add has one field if needed
			$Header = 'Section Content';
			if ($this->ContentTypeID) {
				$Header .= ' ('.$this->ContentType()->getType().')';
			}
			$fields->addFieldToTab(
				'Root.Main',
				HeaderField::create('ContentHeader', $Header)
			);
			if ($this->ContentType()->getCMSFields()->Count()) {
				$fields->addFieldToTab(
					'Root.Main',
					$HasOneButtonField = HasOneButtonField::create('ContentType', 'Content', $this)
				);
				$ButtonText = is_a($this->ContentType(), 'ContentTypeContainer') ? $this->Type().' settings' : 'Edit '.$this->Type();
				$HasOneButtonField
					->getConfig()
					->getComponentByType('GridFieldHasOneEditButton')
					->setButtonName($ButtonText);
				$HasOneButtonField
					->getConfig()
					->getComponentByType('GridFieldDetailForm')
					->setItemRequestClass('ContentTypeGridFieldDetailForm_ItemRequest');
			}

			// add grid if possible
			if (is_a($this->ContentType(), 'ContentTypeContainer')) {
				if ($gridfield = $this->ContentType()->getGridField()) {
					$fields->addFieldToTab('Root.Main', $gridfield);
				}

				if ($fields->dataFieldByName('BackgroundParallax1') || $fields->dataFieldByName('BackgroundParallax2')) {
					$fields->dataFieldByName('BackgroundParallax1')->setRightTitle('can be used for parallax effect on scroll, this sits on top of background image, not implemented in html (use ContentSections.ss)');
					$fields->dataFieldByName('BackgroundParallax2')->setRightTitle('can be used for parallax effect on scroll, this sits on top of the first parallax image, not implemented in html (use ContentSections.ss)');

					$fieldToggle = ToggleCompositeField::create('Parallax', 'Parallax', array(
							$fields->dataFieldByName('BackgroundParallax1'),
							$fields->dataFieldByName('BackgroundParallax2')
						)
					);
					$fields->removeFieldsFromTab('Root.Main', array(
						'BackgroundParallax1',
						'BackgroundParallax2'
					));
					$fields->insertAfter('BackgroundImage', $fieldToggle);
				}

				// remove heading, bg image and links from map and carousel
				if (is_a($this->ContentType(), 'ContentTypeMap') || is_a($this->ContentType(), 'ContentTypeCarousel')) {
					$fields->removeByName('DisplayHeading');
					$fields->removeByName('BackgroundImage');
					$fields->removeByName('BackgroundParallax1');
					$fields->removeByName('BackgroundParallax2');
				}
			}
			$fields->insertAfter('BackgroundImage', TextField::create('BackgroundColour', 'Background Colour (hex)')->setRightTitle('Optional Background Colour'));

			// multiple pages hint
			if ($this->Pages()->Count() > 1) {
				$fields->addFieldToTab(
					'Root.Main',
					HeaderField::create(
						'PageCountHint',
						sprintf(
							'This section is used on multiple pages (%s). Editing it will change it globally!',
							$this->PageTitles()
						)
					)->setHeadingLevel(3),
					'Title'
				);
			}

			// pages grid
			$config = $fields->dataFieldByName('Pages')->getConfig();
			$config->removeComponent($config->getComponentByType('GridFieldAddNewButton'));
			$config->removeComponent($config->getComponentByType('GridFieldEditButton'));
		}

		return $fields;
	}

	public function onBeforeWrite() {
		parent::onBeforeWrite();
		if (!$this->ID && $this->ContentTypeClassName && class_exists($this->ContentTypeClassName)) {
			$ClassName = $this->ContentTypeClassName;
			$ContentType = $ClassName::create();
			$ContentType->write();
			$this->ContentTypeID = $ContentType->ID;
		}
		$this->ContentType()->onAfterWrite(); // needed for caching part, this is not always called otherwise and breaks caching (see ContentSectionPageExtension for method)
	}

	public function onBeforeDelete() {
		parent::onBeforeDelete();
		if ($ContentType = $this->ContentType()) {
			if($ContentType->ID) {
				$ContentType->delete();
			}
		}
		foreach ($this->Links() as $item) {
			$item->delete();
		}
	}

	public function Section() {
		if ($this->ContentTypeID && $ContentType = $this->ContentType()) {

			$this->extend('updateSectionRender', $ContentType);

			return $ContentType->customise(array('Section' => $this))->forTemplate();
		}
	}

	public function LinkModals() {
		return implode(Linkable::get_modals_to_print());
	}

	public function Link($pageID = null) {
		if ($pageID) {
			$page = Page::get()->filter('ID', $pageID)->first();
		} else {
			//not good for menu building
			$page = method_exists(Controller::curr(), 'data') ? Controller::curr()->data() : null;
		}
		return $page ? $page->Link().'#'.$this->ContentType()->TemplateID() : null;
	}

	public function MenuTitle() {
		return $this->Title;
	}

	/**
	 * used by VersionedExtension to recursively publish/unpublish - don't remove
	 *
	 * @return ArrayList
	 */
	public function Children() {
		return ArrayList::create(array($this->ContentType()));
	}
}

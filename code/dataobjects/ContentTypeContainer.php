<?php

class ContentTypeContainer extends ContentType {

	public static function get_gridfieldconfig() {
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewMultiClass(),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(20),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm(),
			new GridFieldOrderableRows('SortID')
		);
		$gridFieldConfig
			->getComponentByType('GridFieldDetailForm')
			->setItemRequestClass('ContentTypeGridFieldDetailForm_ItemRequest');

		$gridFieldConfig
			->getComponentByType('GridFieldAddNewMultiClass')
			->setClasses(singleton(get_called_class())->creatableContentTypes());

		return $gridFieldConfig;
	}

	public function getGridField() {
		// manually loading a hasmanylist as we need a relationlist instance
		// -> calls list->add()
		if(count(singleton(get_called_class())->creatableContentTypes())) {
			$gridField = new GridField(
				'Children',
				$this->getType().' Content',
				HasManyList::create('ContentType', 'ParentID')->forForeignID($this->ID),
				self::get_gridfieldconfig()
			);
			return $gridField;
		}
	}

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		if(!$this->ParentID) {
			// no title needed on top level
			$fields->removeByName('Title');
		} elseif ($gridfield = $this->getGridField()) {
			// grid only on deeper levels as we already got it displayed on the section edit screen
			$fields->addFieldToTab('Root.Main', $gridfield);
		}

		return $fields;
	}

	public function onBeforeDelete() {
		parent::onBeforeDelete();
		foreach($this->Children() as $ContentElement) {
			$ContentElement->delete();
		}
	}

	public function ChildrenContainer(&$List) {
		$Containers = $this->Children()->filterByCallback(function($item) {
			return is_a($item, 'ContentTypeContainer');
		});
		foreach($Containers as $Child) {
			$List[$Child->ID] = $Child->getBreadcrumbs();
			$Child->ChildrenContainer($List);
		}
	}

}
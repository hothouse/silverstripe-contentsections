<?php

class ContentTypeGrid extends ContentTypeContainer {

	private static $db = array(
		'MaxItemsPerRow' => 'Int',
		'ManualColWidth' => 'Boolean'
	);

	private static $defaults = array(
		'MaxItemsPerRow' => 4,
		'ManualColWidth' => false
	);

	private static $allowed_content_types = array(
		'ContentTypeAccordion',
		'ContentTypeCarousel',
		'ContentTypeMap',
		'ContentTypeTabs',
		'ContentTypeImage',
		'ContentTypeText',
		'ContentTypeVideo',
		'ContentTypeImageGallery',
		'ContentTypeFlickrGallery',
		'ContentTypeNewsletterSignUpActiveCampaign',
		'ContentTypeNewsletterSignUpMailchimp',
		'ContentTypeChart'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'ManualColWidth',
				'Column width',
				array(
					'Automatically determine column width based on number of items',
					'Set column width manually on each item'
				)
			)
		);

		$fields->addFieldToTab(
			'Root.Main',
			DropdownField::create(
				'MaxItemsPerRow',
				'Max items per row',
				array(
					2 => 2,
					3 => 3,
					4 => 4,
					6 => 6
				)
			)->hideIf('ManualColWidth')->isEqualTo(1)->end()
		);
		return $fields;
	}

	public function ColWidth() {
		$ColWidth = ceil(12 / $this->Children()->Count());
		if (!$this->MaxItemsPerRow && $defaults = $this->config()->defaults) {
			$this->MaxItemsPerRow = $defaults['MaxItemsPerRow'];
		}
		$ColWidth = max($ColWidth, 12 / $this->MaxItemsPerRow);
		return $ColWidth;
	}

	public function ColWidthFixed() {
		if (!$this->MaxItemsPerRow && $defaults = $this->config()->defaults) {
			$this->MaxItemsPerRow = $defaults['MaxItemsPerRow'];
		}
		return 12 / $this->MaxItemsPerRow;
	}
}
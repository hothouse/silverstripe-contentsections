<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 16/07/15
 * Time: 4:07 PM
 */

class ContentTypeRows extends ContentTypeContainer {
	private static $allowed_content_types = array(
		'ContentTypeGrid',
		'ContentTypeAccordion',
		'ContentTypeCarousel',
		'ContentTypeMap',
		'ContentTypeTabs',
		'ContentTypeImage',
		'ContentTypeSnippetGrid',
		'ContentTypeText',
		'ContentTypeVideo',
		'ContentTypeImageGallery',
		'ContentTypeFlickrGallery',
		'ContentTypeChart'
	);
}
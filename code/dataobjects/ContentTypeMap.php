<?php

class ContentTypeMap extends ContentTypeContainer {

	private static $allowed_content_types = array(
		'ContentTypeMapItem'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		return $fields;
	}

	public function implementFrontend() {
		$List = array(
			MODULE_CONTENTSECTIONS_PATH.'/javascript/markerclusterer.min.js',
			MODULE_CONTENTSECTIONS_PATH.'/javascript/markerwithlabel.js',
			MODULE_CONTENTSECTIONS_PATH.'/javascript/OverlappingMarkerSpiderfier.min.js',
			MODULE_CONTENTSECTIONS_PATH.'/javascript/infobubble.min.js',
			MODULE_CONTENTSECTIONS_PATH.'/javascript/maps.js'
		);
		Requirements::combine_files('mapsections.js', $List);
	}
}
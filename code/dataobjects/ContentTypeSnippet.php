<?php

class ContentTypeSnippet extends ContentType {

	private static $db = array(
		'SubTitle' => 'Varchar(255)',
		'Text' => 'HTMLText'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		// no template chooser if we ust this in a snippet grid
		if($this->Parent()->exists() && $this->Parent()->ClassName == 'ContentTypeSnippetGrid') {
			$fields->removeByName('CustomTemplate');
		}

		$fields->addFieldToTab('Root.Main', new UploadField('Image'), 'Text');
		return $fields;
	}

}
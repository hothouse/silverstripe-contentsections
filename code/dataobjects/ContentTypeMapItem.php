<?php

class ContentTypeMapItem extends ContentType {

	private static $db = array(
		'Text' => 'HTMLText'
	);

	private static $has_one = array(
		'Image' => 'Image'
	);

	public function InfoContent() {
		return Convert::raw2htmlatt($this->renderWith('ContentTypeMapItem'));
	}

}
<?php

/**
 * Created by PhpStorm.
 * User: hothouse
 * Date: 13/07/16
 * Time: 3:43 PM
 */
class ContentTypeChart extends ContentType {

	private static $db = array(
		'ChartValues' => 'Varchar(255)',
		'ChartLabels' => 'Varchar(255)',
		'ChartColours' => 'Varchar(255)',
		'ChartHeight' => 'Int',
		'ChartWidth' => 'Int',
		'DisplayTitle' => 'Boolean',
		'ChartType' => 'enum(\'pie,doughnut,bar\', \'pie\')',
		'LegendPosition' => 'enum(\'top,left,bottom,right\', \'top\')'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		if(!$this->ParentID) {
			$fields->removeByName('Title');
		}

		$fields->addFieldsToTab(
			'Root.Main',
			array(
				TextField::create('ChartValues')->setRightTitle('Comma separated values to be used in chart'),
				TextField::create('ChartColours')->setRightTitle('Comma separated colour hex values to be used in chart, ie. FF8153'),
				NumericField::create('ChartHeight')->setRightTitle('Chart Height in Pixel'),
				NumericField::create('ChartWidth')->setRightTitle('Chart Width in Pixel'),
				DropdownField::create('LegendPosition', 'LegendPosition', singleton('ContentTypeChart')->dbObject('LegendPosition')->enumValues())->setRightTitle('Position of the Chart legend'),
				DropdownField::create('ChartType', 'Chart Type', singleton('ContentTypeChart')->dbObject('ChartType')->enumValues())
			)
		);
		$fields->insertAfter('Title', CheckboxField::create('DisplayTitle')->setRightTitle('Display Title in Front End'));
		return $fields;
	}

	public function forTemplate() {
		return parent::forTemplate();
	}

	protected function getPieData() {
		$numbers = explode(',', $this->ChartValues);
		array_walk($numbers, 'trim');
		array_walk($numbers, 'intval');
		$numbers = array_filter($numbers);

		$colours = explode(',', $this->ChartColours);
		array_walk($colours, 'trim');
		$colours = array_filter($colours);

		$return = new stdClass();
		$returnColours = array();
		$returnHovers = array();

		if (count($numbers)) {
			$i=0;
			foreach ($numbers as $number) {
				$obj = new stdClass();
				$obj->value = $number;

				if (isset($colours[$i])) {
					$colour = trim(str_replace('#', '', $colours[$i]));
				} else {
					$i = 0;
					if (isset($colours[$i])) {
						$colour = trim(str_replace('#', '', $colours[$i]));
					} else {
						$colour = $this->random_color();
					}
				}

				$returnColours[] = '#' . $colour;
				$returnHovers[] = $this->adjustBrightness($colour, -20);

				$i++;
			}
		}

		$return->Numbers = $numbers;
		$return->Colours = $returnColours;
		$return->Hovers = $returnHovers;

		return $return;
	}

	public function PieNumbers() {
		$pieData = $this->getPieData();
		return json_encode($pieData->Numbers);
	}

	public function PieColours() {
		$pieData = $this->getPieData();
		return json_encode($pieData->Colours);
	}

	public function PieHovers() {
		$pieData = $this->getPieData();
		return json_encode($pieData->Hovers);
	}

	protected function PieLabels() {
		$labels = explode(',', $this->ChartLabels);
		array_walk($labels, 'trim');
		$labels = array_filter($labels);
		return json_encode($labels);
	}

	protected function random_color_part() {
		return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	protected function random_color() {
		return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

	protected function adjustBrightness($hex, $steps) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max(-255, min(255, $steps));

		// Normalize into a six character long hex string
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		}

		// Split into three parts: R, G and B
		$color_parts = str_split($hex, 2);
		$return = '#';

		foreach ($color_parts as $color) {
			$color   = hexdec($color); // Convert to decimal
			$color   = max(0,min(255,$color + $steps)); // Adjust color
			$return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
		}

		return $return;
	}

	public function implementFrontend() {
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH .'/javascript/Chart.min.js');
	}
}
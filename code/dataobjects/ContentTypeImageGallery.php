<?php

class ContentTypeImageGallery extends ContentTypeContainer {

	private static $db = array(
		'AutoStart' => 'Boolean',
		'Transition' => "Enum('slide, fade', 'slide')"
	);

	private static $defaults = array(
		'AutoStart' => true
	);

	private static $allowed_content_types = array(
		'ContentTypeImage'
	);

	public static function get_gridfieldconfig() {
		$gridFieldConfig = parent::get_gridfieldconfig();
		$gridFieldConfig->addComponent(new GridFieldBulkUpload());
		$gridFieldConfig->getComponentByType('GridFieldBulkUpload')->setUfSetup('setFolderName', 'ImageGallery');
		return $gridFieldConfig;
	}

	public function getGridField() {
		// manually loading a hasmanylist as we need a relationlist instance
		// -> calls list->add()
		$gridField = new GridField(
			'Children',
			$this->getType().' Content',
			HasManyList::create('ContentTypeImage', 'ParentID')->forForeignID($this->ID),
			self::get_gridfieldconfig()
		);
		return $gridField;
	}

	public function implementFrontend() {
		Requirements::css(MODULE_CONTENTSECTIONS_PATH.'/css/justifiedGallery.min.css');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/jquery.mobile.custom.min.js');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/justifiedgallery.js');
		Requirements::javascript(MODULE_CONTENTSECTIONS_PATH.'/javascript/imagegallery.js');
	}
}
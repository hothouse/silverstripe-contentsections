<?php

class ContentTypeText extends ContentType {

	private static $db = array(
		'DisplayHeading' => 'Boolean',
		'Text' => 'HTMLText'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		if(!$this->Parent()->exists()) {
			// as a text content section we don't need a title at all
			$fields->removeByName('Title');
			$fields->removeByName('DisplayHeading');
		} elseif($this->Parent()->exists() && is_a($this->Parent(), 'ContentTypeTabs')) {
			// within tabs we always need the title
			$fields->removeByName('DisplayHeading');
		} else {
			// normal behavior
			$fields->addFieldToTab('Root.Main', CheckboxField::create('DisplayHeading'), 'Title');
			$fields->dataFieldByName('Title')->displayIf('DisplayHeading')->isChecked()->end();
		}

		return $fields;
	}

}
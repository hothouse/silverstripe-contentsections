<?php

class ContentTypeTabs extends ContentTypeContainer {

	private static $db = array(
		'TabsPosition' => "Enum('top, left, right, bottom', 'top')"
	);

	private static $allowed_content_types = array(
		'ContentTypeSnippetGrid',
		'ContentTypeGrid',
		'ContentTypeRows',
		'ContentTypeMap',
		'ContentTypeImage',
		'ContentTypeText',
		'ContentTypeVideo',
		'ContentTypeAccordion',
		'ContentTypeImageGallery',
		'ContentTypeFlickrGallery'
	);

}
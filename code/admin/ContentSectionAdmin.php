<?php

class ContentSectionAdmin extends ModelAdmin {

	private static $managed_models = array('ContentSection');
	private static $url_segment = 'contentsections';
	private static $menu_title = 'Content Sections';
	private static $model_importers = array();

	public function getEditForm($id = null, $fields = null) {
		$form = parent::getEditForm($id, $fields);

		$gridField = $form->Fields()->fieldByName($this->sanitiseClassName($this->modelClass));
		$list = $gridField->getList();
		if (class_exists('Subsite')) {
			$list = $list->filter(array('SubsiteID' => Subsite::currentSubsiteID()));
		}
		$gridField->setList($list);
		$gridField->getConfig()->removeComponentsByType('GridFieldAddNewButton');
		return $form;
	}

	public function subsiteCMSShowInMenu(){
		return true;
	}

}
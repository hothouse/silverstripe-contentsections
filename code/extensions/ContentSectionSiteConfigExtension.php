<?php

class ContentSectionSiteConfigExtension extends DataExtension {

	private static $db = array(
		'ContentSectionWhiteList' => 'Text',
		'ContentTypeWhiteList' => 'Text',
		'ContentTypeFrontendUses' => 'Text'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab(
			'Root.ContentSections',
			new CheckboxSetField(
				'ContentSectionWhiteList',
				'Allowed content section types',
				singleton('ContentType')->creatableContentTypes()
			)
		);
		$fields->addFieldToTab(
			'Root.ContentSections',
			new CheckboxSetField(
				'ContentTypeWhiteList',
				'Allowed content types',
				singleton('ContentType')->creatableContentTypes()
			)
		);

		$fields->removeByName('ContentTypeFrontendUses');
	}

}

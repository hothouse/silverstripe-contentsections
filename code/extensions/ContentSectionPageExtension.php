<?php

class ContentSectionPageExtension extends DataExtension {

	private static $db = array(
		'PositionContentAfterSectionID' => 'Int',
		'UseSectionsAsChildren' => 'Boolean'
	);

	private static $many_many = array(
		'ContentSections' => 'ContentSection'
	);

	private static $many_many_extraFields = array(
		'ContentSections' => array('SortID' => 'Int')
	);

	public function updateCMSFields(FieldList $fields) {
		// content position
		if ($this->owner->ContentSections()->Count()) {
			$PositionMap = array();
			if ($this->owner->ClassName != 'BlogPost') {
				$PositionMap[-2] = '- do not display at all -';
			}
			$PositionMap[0] = '- always position on top -';
			$PositionMap += $this->owner->ContentSections()->sort('SortID')->map()->toArray();
			$PositionMap[-1] = '- always position on bottom -';
			$fields->addFieldToTab(
				'Root.Main',
				DropdownField::create(
					'PositionContentAfterSectionID',
					'Display Page Content After Section',
					$PositionMap
				),
				'Content'
			);
			$fields->dataFieldByName('Content')->hideUnless('PositionContentAfterSectionID')->isNotEqualTo(-2)->end();
		}

		// add content sections
		$config = GridFieldConfig_RelationEditor::create(100);
		if (class_exists('GridFieldOrderableRows')) {
			$config->addComponent(new GridFieldOrderableRows('SortID'));
		}
		$CreatableContentSectionTypes = singleton('ContentType')->creatableContentSectionTypes();
		if (!count($CreatableContentSectionTypes)) {
			$config->removeComponentsByType('GridFieldAddNewButton');
		}
		$config->getComponentByType('GridFieldAddExistingAutocompleter')->setSearchFields(array('Title:PartialMatch'));
		$gridField = new GridField(
			'ContentSections',
			'Content Sections',
			$this->owner->ContentSections(),
			$config
		);
		$fields->addFieldToTab('Root.ContentSections', CheckboxField::create('UseSectionsAsChildren'));
		$fields->addFieldToTab('Root.ContentSections', $gridField);
	}

	public function ContentSectionList() {
		return $this->owner->ContentSections()
			->filter('IsActive', 1)
			->exclude('ContentTypeID', 0)
			->sort('SortID');
	}

	public function DisplayLayout() {
		return $this->owner->PositionContentAfterSectionID > -2;
	}

	public function ContentSectionTemplate($position = 'before') {
		if (
			$this->owner->PositionContentAfterSectionID == 0 && $position == 'before'
			|| ($this->owner->PositionContentAfterSectionID == -1 && $position == 'after')
		) {
			return null;
		}

		$include = $position == 'before' || $this->owner->PositionContentAfterSectionID == 0;
		$ContentSectionList = ArrayList::create();
		$frontendScripts = SiteConfig::current_site_config()->ContentTypeFrontendUses;
		if (strlen($frontendScripts)) $frontendScripts = unserialize($frontendScripts);
		foreach ($this->ContentSectionList() as $ContentSection) {
			if ($include) {
				$ContentSectionList->push($ContentSection);
			}
			if ($ContentSection->ID == $this->owner->PositionContentAfterSectionID) {
				$include = !$include;
			}
			if (isset($frontendScripts[$ContentSection->ID])) {
				foreach ($frontendScripts[$ContentSection->ID] as $id) {
					$obj = ContentType::get()->filter('ID', $id)->first();
					if ($obj) {
						$obj->implementFrontend();
					}
				}
			}
		}

		return ArrayData::create(array(
				'ContentSections' => $ContentSectionList,
				'AbsoluteLink' => $this->owner->AbsoluteLink(),
				'LastPageEdit' => $this->owner->LastEdited
		))->renderWith('ContentSections');
	}

	public function Children() {
		if ($this->owner->UseSectionsAsChildren) {
			return $this->ContentSectionList();
		} else {
			return $this->owner->stageChildren(false);
		}
	}

	// used to display meta data
	public function ContentSectionImages() {
		$Images = ArrayList::create();
		$ContentTypes = ContentType::get()->byIDs($this->ContentSectionList()->column('ContentTypeID'));
		foreach ($ContentTypes as $ContentType) {
			$Images->merge(
				ContentType::get()
					->filter('ClassName', array('ContentTypeImage', 'ContentTypeSnippet'))
					->byIDs($ContentType->getDescendantIDList())
			);
		}
		return $Images;
	}

	// used to display meta data
	public function ContentSectionBackgroundImages() {
		return Image::get()->byIDs($this->ContentSectionList()->column('BackgroundImageID'));
	}
}
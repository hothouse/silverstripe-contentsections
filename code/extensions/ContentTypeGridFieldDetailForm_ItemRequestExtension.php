<?php

class ContentTypeGridFieldDetailForm_ItemRequestExtension extends DataExtension {

	// limit the content type choices
	function updateItemEditForm(Form $form) {

		// adjust createable content types
		if ($form->record->ClassName == 'ContentType' && $form->record->ParentID && $Parent = $form->record->Parent()) {
			if ($field = $form->fields->dataFieldByName('ClassName')) {
				$field->setSource($Parent->creatableContentTypes());
			}
		}

		// make parent id form field a changeable field
		// the original sets this read only
		if ($form->record->ID && is_a($form->record, 'ContentType')) {
			$form->fields()->removeByName('ParentID');
			$TopLevelID = $form->record->getTopLevelID();
			$CurrentContentSection = ContentSection::get()->filter('ContentTypeID', $TopLevelID)->first();

			$form->fields()->addFieldToTab(
				'Root.Settings',
				HeaderField::create('ParentHint', 'This element belongs to:')
			);

			// section drop down
			$ContentSectionMap = array();
			$ContentSections = ContentSection::get()->filterByCallback(function($ContentSection) {
				$ContentType = $ContentSection->ContentType();
				return $this->owner->record->ID == $ContentType->ID || !$ContentType->ID || is_a($ContentType, 'ContentTypeContainer');
			});
			foreach($ContentSections as $ContentSection) {
				$Pages = $ContentSection->PageTitles();
				$Pages = $Pages ? ' (on '.$Pages.')' : ' (no pages assigned)';
				$ContentSectionMap[$ContentSection->ID] = $ContentSection->Title.' / '.$ContentSection->ContentType()->getType().' - '.$Pages;
			}
			$SectionDropdown = DropdownField::create(
				'ContentSectionID',
				'Content Section',
				$ContentSectionMap,
				$CurrentContentSection->ID
			);
			$form->fields()->addFieldToTab(
				'Root.Settings',
				$SectionDropdown
			);

			// container drop down
			$ContainerDropdown = DependentDropdownField::create(
				'ParentID',
				'Container',
				array('ContentType', 'get_content_containers_for_section'),
				$form->record->ParentID
			)->setDepends($SectionDropdown);
			$ContainerDropdown->setForm($form);
			$form->fields()->addFieldToTab(
				'Root.Settings',
				$ContainerDropdown
			);
		}
	}

}
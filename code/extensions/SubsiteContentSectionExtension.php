<?php

class SubsiteContentSectionExtension extends DataExtension {

	private static $has_one = array(
		'Subsite' => 'Subsite'
	);

	public function updateCMSFields(FieldList $fields) {

		// pages grid
		$config = $fields->dataFieldByName('Pages')->getConfig();
		$field = $config->getComponentByType('GridFieldAddExistingAutocompleter');
		$field->setSearchList(Page::get()->filter('SubsiteID', Subsite::currentSubsiteID()));

		// hidden field
		$fields->push(new HiddenField('SubsiteID', 'SubsiteID', Subsite::currentSubsiteID()));
	}

}
<?php

class Linkable extends DataExtension {

	private static $db = array(
		'LinkTitle' => 'Varchar',
		'LinkType' => "Enum(array('None', 'Modal', 'Page', 'External', 'Phone', 'Content Element'), 'None')",
		'ModalContent' => 'HTMLText',
		'ExternalLink' => 'Varchar(255)',
		'Phone' => 'Varchar',
		'OpenInNewTab' => 'Boolean'
	);

	private static $has_one = array(
		'LinkedPage' => 'SiteTree',
		'LinkedContentType' => 'ContentType'
	);

	// collects all modals to print in the markup
	private static $modals_to_print = array();

	public function updateCMSFields(FieldList $fields) {
		$fields->removeByName('LinkedPageID');
		$fields->removeByName('LinkedContentTypeID');

		// page drop down
		$PageDropdown = TreeDropdownField::create('LinkedPageID', 'Linked Page', 'SiteTree');

		// Set default value as the DependentDropdownField class calls the getHasEmptyDefault method on the drop down
		// it depends on if it doesnt have a value. In this case we depend on a TreeDropdownField which doesn't have
		// this method. Set default to prevent this call. Good workaround: yes. Elegant: not so sure.
		if (!$this->owner->LinkedPageID) {
			$this->owner->LinkedPageID = 1;
		}

		$ContentTypeDropdown = DependentDropdownField::create(
			'LinkedContentTypeID',
			'ContentElement',
			array('ContentType', 'get_content_elements_for_page')
		)->setDepends($PageDropdown);

		$fields->addFieldsToTab(
			'Root.Main',
			array(
				TextField::create('ExternalLink')->displayIf('LinkType')->isEqualTo('External')->end(),
				TextField::create('Phone')->displayIf('LinkType')->isEqualTo('Phone')->end(),
				DisplayLogicWrapper::create($PageDropdown)
					->displayIf('LinkType')->isEqualTo('Page')
					->orIf('LinkType')->isEqualTo('Content Element')
					->end(),
				CheckboxField::create('OpenInNewTab')
					->displayIf('LinkType')->isEqualTo('Page')
					->orIf('LinkType')->isEqualTo('External')
					->orIf('LinkType')->isEqualTo('Content Element')
					->end(),
				HtmlEditorField::create('ModalContent')->displayIf('LinkType')->isEqualTo('Modal')->end(),
				$ContentTypeDropdown
					->displayIf('LinkType')->isEqualTo('Content Element')
					->end()
			)
		);
	}

	public function ExternalLink() {
		$Link = $this->owner->getField('ExternalLink');
		if ((strpos($Link, 'tel') !== 0) && (strpos($Link, 'mailto') !== 0) && (strpos($Link, 'http') !== 0)) {
			$Link = 'http://'.$Link;
		}
		return $Link;
	}

	public function LinkedContentTypeLink() {
		return $this->owner->LinkedPage()->Link('#'.$this->owner->LinkedContentType()->TemplateID(true));
	}

	public function LinkTitle() {
		if(!$this->HasLink()) {
			return '';
		} elseif($this->owner->LinkType == 'Modal' && $Title = $this->owner->getField('Title')) {
			return $Title;
		} elseif ($this->owner->getField('LinkTitle')) {
			return $this->owner->getField('LinkTitle');
		} elseif ($this->owner->LinkedPageID) {
			return $this->owner->LinkedPage()->Title;
		}
	}

	public function HasLink() {
		return $this->owner->LinkType != 'None';
	}

	public function LinkOpen($attributes = null) {
		if($this->HasLink()) {

			// make sure we print the modal
			if($this->owner->LinkType == 'Modal') {
				self::$modals_to_print[$this->owner->ID] = $this->owner->renderWith('LinkModal');
			}

			// render
			return $this->owner->customise(array('Attributes' => $attributes))->renderWith('LinkOpen');
		}
	}

	public function LinkClose() {
		if($this->HasLink()) {
			return '</a>';
		}
	}

	public static function get_modals_to_print() {
		$Modals = self::$modals_to_print;
		self::$modals_to_print = array();
		return $Modals;
	}
}